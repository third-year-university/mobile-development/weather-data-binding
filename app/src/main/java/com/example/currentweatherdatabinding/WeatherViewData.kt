package com.example.currentweatherdatabinding

class WeatherViewData() {
    companion object{
        val imgIds = mapOf(
            Pair("01d", R.drawable.i01d),
            Pair("02d", R.drawable.i02d),
            Pair("03d", R.drawable.i03d),
            Pair("04d", R.drawable.i04d),
            Pair("09d", R.drawable.i09d),
            Pair("10d", R.drawable.i10d),
            Pair("11d", R.drawable.i11d),
            Pair("13d", R.drawable.i13d),
            Pair("50d", R.drawable.i50d),
            Pair("01n", R.drawable.i01n),
            Pair("02n", R.drawable.i02n),
            Pair("03n", R.drawable.i03n),
            Pair("04n", R.drawable.i04n),
            Pair("09n", R.drawable.i09n),
            Pair("10n", R.drawable.i10n),
            Pair("11n", R.drawable.i11n),
            Pair("13n", R.drawable.i13n),
            Pair("50n", R.drawable.i50n)
            )
        val windIcons = arrayOf(
            R.drawable.w0,
            R.drawable.w30,
            R.drawable.w45,
            R.drawable.w60,
            R.drawable.w90,
            R.drawable.w120,
            R.drawable.w120,
            R.drawable.w135,
            R.drawable.w150,
            R.drawable.w180,
            R.drawable.w210,
            R.drawable.w240,
            R.drawable.w270,
            R.drawable.w300,
            R.drawable.w315,
            R.drawable.w330
            )
    }
    var dayLengthMinutes: Int = 0
    var dayLengthHours: Int = 0
    var iconID: Int = 0
    var windIconID: Int = 0
    var dayLengthStr: String = ""
    var dayLength: Int = 0
        set(value) {
            dayLengthMinutes = value / 60
            dayLengthHours = value / 3600
            dayLengthStr = "Продолжительность дня: $dayLengthHours часов, $dayLengthMinutes минут"
            field = value
        }


    var windDegrees: Int = 0
        set(value){
            field = value
            windIconID = windIcons[0]
            for (i in 1..15){
                if (value <= (i + 1) * 22.5 + 11.25 && value >= i * 22.5 - 11.25){
                    windIconID = windIcons[i]
                }
            }
        }
    var weatherIcon: String = ""
        set(value) {
            field = value
            iconID = if (!imgIds.containsKey(value)){
                R.drawable.none
            } else{
                imgIds[value]!!
            }
        }
}
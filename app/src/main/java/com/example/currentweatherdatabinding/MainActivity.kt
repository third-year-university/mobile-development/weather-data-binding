package com.example.currentweatherdatabinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var editText: EditText
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding  = DataBindingUtil.setContentView(this, R.layout.activity_main)
        editText = findViewById(R.id.editTextTextPersonName)
        textView = findViewById(R.id.textView)
    }
    suspend fun loadWeather() {
        val city = editText.text.toString()
        Log.d("NIKITA", city)
        val API_KEY = resources.getString(R.string.api_key)
        val weatherURL = "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$API_KEY&units=metric";
        try {
            val stream = URL(weatherURL).getContent() as InputStream
            // JSON отдаётся одной строкой,
            val data = Scanner(stream).nextLine()
            Log.d("mytag", data)
            val weatherData: WeatherData = Gson().fromJson<WeatherData>(data, WeatherData::class.java)
            val weatherViewData = WeatherViewData()
            weatherViewData.dayLength = weatherData.sys?.sunset!! - weatherData.sys?.sunrise!!
            weatherViewData.windDegrees = weatherData.wind?.deg!!
            weatherViewData.weatherIcon = weatherData.weather[0].icon!!
            binding.weather = weatherViewData
        }
        catch (e: java.lang.Exception){
            Log.d("NIKITA",
                e.toString())
            textView.text = "Произошла ошибка, попробуйте ещё раз"
        }
    }
    public fun onClick(v: View) {
        // Используем IO-диспетчер вместо Main (основного потока)
        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }
}